## Code for part 1

The driver code is located in the *./code/* directory in the *Driver_LinAl.f90* file

To run the code
```bash
cd code/
```

compile with 
```bash
make
```

then the output is located in the text file

```bash
code/output.txt
```


## Reports

I have the report for code *Part 1* in *assignment_04/part1_code_report.pdf* and the
theory *Part 2* in *assignment_04/part2_theory_report.pdf*
