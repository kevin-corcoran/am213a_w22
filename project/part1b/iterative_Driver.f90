program iterative_Driver 

  use LinAl

  implicit none

  real, dimension(:,:), allocatable :: A, D, b, x_0, x
  ! double precision, dimension(:), allocatable :: b
  integer :: m, selection, i
  real :: acc, D_input

  m = 100
  acc = 10.0**(-5)
  D_input = 0.0
  ! Build matrices
  allocate(A(m, m)) ! Ones on off diagonal
  A(1:m,1:m) = 1.0
  forall (i = 1:m) A(i,i) = 0.0 ! user will enter diagonal elements

  allocate(b(m, 1)) ! b_i = i
  b = 0.0
  do i = 1,m
    b(i,1) = i ! type issues?
  enddo

  ! Run the code for a 10 x 10 matrix for D_ii = 2, 5, 10, 100, 1000 (user will enter these values)
  allocate(D(m,m)) ! must be strictly diagonally dominant
  D(1:m,1:m) = 0.0

  allocate(x(m,1)) ! Solution returned by algorithm
  x = 0.0

  allocate(x_0(m,1))
  forall (i = 1:m) x_0(i,1) = 1.0 ! initial guess

  ! Prompt user for diagonal elements
  print *, "Enter a number for the diagonal elements of A (eg 2, 5, 10, 100, 1000, -1)"
  print *
  read(*,*) D_input
  print *, "Your input: ", D_input
  print *

  if (D_input == -1) then
    print *, "input was -1: setting a_ii = i"
    forall (i = 1:m) D(i,i) = i
  else
    forall (i = 1:m) D(i,i) = D_input
  endif


  ! Add diagonal to A and deallocate D
  A = A + D
  deallocate(D)

  ! Prompt user to select algorithm
  ! print *, "Select algorithm to solve Ax = b (1, 2, or 3): "
  print *, "1. Jacobi"
  print *, "2. Gauss-Seidel"
  print *, "3. Conjugate Gradient"
  print *, "Select algorithm to solve Ax = b (1, 2, or 3): "
  print *

  do while (.not. ((selection == 1) .or. (selection == 2) .or. (selection == 3) ))
    read(*,*) selection
    
    if (selection == 1) then
      print *, "You picked Jacobi"

      print *, "Running algorithm.."
      call Jacobi(A, b, x_0, x, acc)
      ! print solution
      print *, "Solution"
      call print_mat(x)
  x = 0.0
    else if (selection == 2) then
      print *, "You picked Gauss-Seidel"

      print *, "Running algorithm.."
      call GaussSeidel(A, b, x_0, x, acc)
      ! print solution
      print *, "Solution"
      call print_mat(x)
    else if (selection == 3) then
      print *, "You picked Conjugate Gradient"

      print *, "Running algorithm.."
      call ConjugateGradient(A, b, x_0, x, acc)
      ! print solution
      print *, "Solution"
      call print_mat(x)
    ! else 
    !   print *, "Please pick number 1, 2, or 3"
    !   print *
    end if
  end do
end program iterative_Driver
