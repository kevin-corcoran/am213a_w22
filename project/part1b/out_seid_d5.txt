 Enter a number for the diagonal elements of A (eg 2, 5, 10, 100, 1000, -1)

 Your input:    5.0000000000000000     

 1. Jacobi
 2. Gauss-Seidel
 3. Conjugate Gradient
 Select algorithm to solve Ax = b (1, 2, or 3): 

 You picked Gauss-Seidel
 Running algorithm..
 number of iterations: 
          16
 Solution

   10    x    1
    -0.7321
    -0.4821
    -0.2321
     0.0179
     0.2679
     0.5179
     0.7679
     1.0179
     1.2679
     1.5179
 
