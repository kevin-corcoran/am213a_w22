using Plots

# Pull actual data points
data = readlines("./least_squares_data.dat")
x_act = []
y_act = []
for i = 1:length(data)
    s = rsplit(data[i],'\t')
    append!(x_act, parse(Float64,s[1]))
    append!(y_act, parse(Float64,s[2]))
end

deg5 = readlines("./interp3_house.dat")
xs = []
ys = []
l = length(deg5)
for i = 1:l
    s = split(deg5[i])
    append!(xs, parse(Float64,s[1]))
    append!(ys, parse(Float64,s[2]))
end
plot(xs, ys, label="deg(3) interpolant")
scatter!(x_act,y_act, label="data pts")
title!("Degree 3 Vandermonde Interpolation")
savefig("../figures/Householder_p3")


    
# Checking work
using LinearAlgebra
using SpecialMatrices
V = Vandermonde(x_act)
V = V[:,1:6]
S = V'*V
S = Float64.(S)

# V'Vx = V'y
y = V'y_act

C = cholesky(S)
U = C.U

V = Float64.(V)
x = inv(V'*V)*V'y_act

using Polynomials
# poly(x)
P = polyfit(x_act, y_act, 5)

xi = collect(0:10)*(1/10)

for i in xi
    println(polyval(P, i))
end
