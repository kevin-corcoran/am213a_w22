## SVD code

All code is located in the directory *./part1a/*, the driver code is in
*./part1a/image_Driver.f90*

To run the code
```bash
cd part1a/
```

compile with 
```bash
make
```

then the output is located in the text file

```bash
part1a/output.txt
```

and image data is reconstructed using singular value decomposition and output
to files *Ak_[#].dat* where the number *[#]* is the number of singular values
used in the reconstruction.

The plotting routine is in the file *./part1a/plotter.py* and can be run with

```bash
python3 plotter.py
```

## Jacobi, Gauss-Seidel, Conjugate Gradient

This code is located in the directory *./part1b*, and the driver code is in
*./part1b/iterative_Driver.f90*


first
```bash
cd part1a/
```

compile with 
```bash
make
```

this creates the executable *output*, run with

```bash
./output
```

and a list of options will be presented, d = 2, 5, 10, 100, 1000, -1. Where -1
presents the matrix of all 1's and a_ii = i on the diagonal. I ran the code
with various input files and sent the output to correspondingly named files,
for example

```bash
./output < input_CG_aii > out_CG_aii_m10.txt
```

used the option d = -1 with the conjugate gradient algorithm for a 10 x 10 matrix and the output was written to the file *out_CG_aii_m10.txt*

Plotting is done with julia this time in the file *.part1b/julPlot.jl* and it
used the *Plots* package.

If you want to run this file, first start the julia real and type
```bash
]
```

then add the *Plots* package if you don't have it
```bash
add Plots
```

and you can then run the code with
```bash
julia julPlot.jl
```

This will produce *.png* images of the plots in the directory *./part1b/*.
Although I manually moved all plotting images, from both part1a and part1b,
into the *./figures/* directory.
