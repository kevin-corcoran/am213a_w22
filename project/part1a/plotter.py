import numpy as np
import matplotlib.pyplot as plt


# Show original (high resolution) image in its own figure
Ah = np.loadtxt("dog_bw_data.dat")
fig0 = plt.figure(figsize=(25,10))
plt.imshow(Ah, cmap="gray")
plt.title("Original")


# Show low resolution image in its own figure
A0 = np.loadtxt("dog_bw_data_LR.dat")
fig1 = plt.figure(figsize=(25,10))
plt.imshow(A0, cmap="gray")
plt.title("Low resolution")

m, n = np.shape(Ah)
mn = m*n

# ks = [20, 40, 80, 160, 320, 640]
ks = [20, 40, 80, 160, 320, 640, 1280, 2560]

# Figure for all image approximations
fig = plt.figure(figsize=(35,20))

i = 0
E = [] # norms
for k in ks:
    i = i + 1

    filename = f'Ak_{k}.dat'
    A = np.loadtxt(filename)

    l = len(A)
    A = A.reshape(m, n) #, order='C') # 'C', 'F', 'A'

    err = np.linalg.norm(A-Ah, ord='fro')/mn
    if (err < 10**(-3)):
        print(f"for k = {k} the error is less than 10^-3")
    E.append(err)

    plt.subplot(4,2, i)
    plt.imshow(A, cmap="gray")
    plt.title(f"Rank {k} Approximation")

# Plot error in a new figure
fig_err = plt.figure(figsize=(25,10))
plt.scatter(ks, E)
plt.title("Averaged Frobenius norm")

# show all figures
plt.show()


