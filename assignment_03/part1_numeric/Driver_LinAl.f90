! TODO: make seperate drivers for part 1 (problem 1 and 2)
Program Driver_LinAl

  use LinAl

  implicit none
  
  character(len=100) :: myFileName
  integer :: i, j

  ! Variables for coding problem 1 (cholesky solution of least squares)
  ! TODO: change n -> m, add: n = d+1
  integer :: n, d ! n: number of data points; d: degree of polynomial
  ! TODO: change name to x_data, y_data, y_dcopy
  real, dimension(:,:), allocatable :: xs, ys, ys_copy ! data points x and y
  real, dimension(:,:), allocatable :: V, V_copy ! Vandermonde matrix
  ! TODO?: change name?
  real, dimension(:,:), allocatable :: S
  real, dimension(:,:), allocatable :: X ! Solution
  ! TODO: change to xs, ys
  real, dimension(:), allocatable :: xs_curve, ys_curve ! evaluation points for polynomial interp.

  ! Variables for coding problem 2 (QR)
  real, dimension(:,:), allocatable :: Q, R, S_copy, Id
  real, dimension(:,:), allocatable :: dR ! diagonal elements of R
  real, dimension(:,:), allocatable :: P ! projection onto columns of S

  real :: err, a, b
  integer :: N_p
  logical :: SINGLR
  logical :: flag
  SINGLR = .FALSE.
  flag = .FALSE.

  ! Read data
  myFileName = 'least_squares_data.dat'

  ! TODO: write a subroutine for dynamic arrays/file length (linked lists?)
  n = 21
  allocate(xs(n,1))
  allocate(ys(n,1))
  xs = 0.0 ! data points xs
  ys = 0.0 ! data points f(xs)
  open (10, file=myFileName)
  i=0
  do while (.true.)
    i=i+1
    read (10, *, end=999) xs(i,1), ys(i,1)
  enddo
  999 continue

  ! n = i
  ! allocate(xs(n,1))
  ! allocate(ys(n,1))

  ! TODO: make this a subroutine
  ! Build vandermond matrix
  d = 5 ! degree of polynomial
  allocate(V(n, d+1))
  allocate(V_copy(n, d+1))
  allocate(S(d+1, d+1))
  allocate(S_copy(d+1, d+1))
  S = 0.0

  do i = 1,n
    do j = 1,d+1
      V(i,j) = xs(i,1)**(j-1)
    enddo
  enddo

  print *, "Vandermonde Matrix"
  call print_mat(V)


  ! normal equations
  S = matmul(transpose(V),V)
  ! create a copy 
  ys_copy = ys
  S_copy = S
  ys = matmul(transpose(V),ys)

  print *, "Normal matrix V^TV"
  call print_mat(S)

  !call choleskyDecom(S, flag)

  print *, "Choleskty decomp"
  call print_mat(S)

  allocate(X(size(S,dim=1),size(ys,dim=2)))
  X = 0.0 ! Solution
  call backSubChol(S,ys, X, size(S,dim=1))

  print *, "Solution to Vx = y for polynomial interpolation (the coefficents)"
  call print_mat(X)

  ! Error in Vx = y
  err = 0.0
  print *, "Error ||Vx-y||_2"
  call two_norm(matmul(V,X)- ys_copy, size(ys_copy,dim=1), err)
  print *, err

  ! TODO?: make this a subroutine (a,b, N, d, coefficients)
  ! Computes fitted curve and prints it to a file
  ! f(x) = X(1)+X(2)x+X(3)x^2+X(4)x^3+X(5)x^4+X(6)x^5

  N_p = 101 ! Number of data points
  a = 0.0; b = 1.0 ! TODO: a: min y_data b: max y_data
  allocate(xs_curve(N_p))
  allocate(ys_curve(N_p))
  xs_curve = 0.0
  ys_curve = 0.0

  do i = 1,N_p
    ! (1-0)/100 ! step size
    ! X(1)+X(2)x+X(3)x^2+X(4)x^3+X(5)x^4+X(6)x^5
    xs_curve(i) = 0 + (i-1)*((b-a)/(N_p-1))
    do j = 1,d + 1
      ys_curve(i) = ys_curve(i) + X(j,1)*xs_curve(i)**(j-1)
    enddo
  enddo

  ! TODO: name file according to value of d
  open(10,file="interp5.dat")

  do i=1,101
     write(10,*) xs_curve(i), ys_curve(i)
  enddo

  close(10)






  ! Part 1 (Problem 2 - QR)

  allocate(Id(n,n)) ! identity matrix
  Id(1:n,1:n) = 0.0
  forall (i = 1:n) Id(i,i) = 1.0
  ! allocate(P(d+1, n)) ! Projection matrix
  ! P = 0.0
  allocate(Q(n, n)) ! Q matrix
  Q = 0.0
  allocate(R(n, d+1)) ! R matrix
  R = 0.0
  allocate(dR(d+1, 1))
  dR = 0.0

  !allocate(V(n, d+1))
  !y_data

  V_copy = V
  print *, "Vandermonde matrix"
  call print_mat(V)
  ! Perform QR decomposition on V
  call householderQR(V, dR, [n, d+1])
  print *, "A_qr"
  call print_mat(V)

  call expliciteQR(V, dR, Q, R)

  ! print *, "Q"
  ! call print_mat(Q)
  ! print *, "R"
  ! call print_mat(R)
  ! print *, "Identity Q'Q"
  ! call print_mat(matmul(transpose(Q),Q))
  ! print *, "V = QR"
  ! call print_mat(matmul(Q,R))
  ! print *, "Projection matrix"
  ! P = matmul(Q(:,1:d+1), transpose(Q(:,1:d+1)))
  ! call print_mat(P)


  ! calculate Q^Ty
  ys = ys_copy
  call householderQTy(V, ys)
  ys = ys(1:d+1,:)
  ! same as above
  ! ys = matmul(transpose(Q(:,1:d+1)), ys)

  print *, "Q^ty"
  call print_mat(ys)

  ! solve for least square solution x
  X = 0.0
  call backSubstitutionU(R(1:d+1,:), ys, X)

  print *, "Solution - coefficients to polynomial"
  call print_mat(X)
  

  ! Error in Vx = y
  V = V_copy
  err = 0.0
  print *, "Error ||Vx-y||_2"
  call two_norm(matmul(V,X)- ys_copy, size(ys_copy,dim=1), err)
  print *, err

  V = V_copy

  print *
  print *, "V - QR"
  call print_mat(V - matmul(Q,R))

  print *, "Q'Q - I"
  call print_mat(matmul(transpose(Q),Q) - Id)

  print *, "2 norm error Q'Q - I"
  call printColumnNorm(matmul(transpose(Q),Q) - Id)

  xs_curve = 0.0
  ys_curve = 0.0

  do i = 1,N_p
    ! (1-0)/100 ! step size
    ! X(1)+X(2)x+X(3)x^2+X(4)x^3+X(5)x^4+X(6)x^5
    xs_curve(i) = 0 + (i-1)*((b-a)/(N_p-1))
    do j = 1,d + 1
      ys_curve(i) = ys_curve(i) + X(j,1)*xs_curve(i)**(j-1)
    enddo
  enddo

  ! TODO: name file according to value of d
  open(10,file="interp5_house.dat")

  do i=1,101
     write(10,*) xs_curve(i), ys_curve(i)
  enddo

  close(10)


  ! TODO: Deallocate arrays

end Program Driver_LinAl
