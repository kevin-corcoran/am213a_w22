
!! /codes/lapack/solve1.f90
!! This routine solves Ax=b using DGESV routine in LAPACK.
!!
!! Recall that the naming convention follows (see http://www.netlib.org/lapack/lug/node26.html)
!!   D:  double precision
!!   GE: general type of matrix
!!   SV: simple driver by factoring A and overwriting B with the solution x
!!
!! See also:
!! a. https://software.intel.com/sites/products/documentation/doclib/mkl_sa/11/mkl_lapack_examples/dgesv.htm
!! b. http://www.netlib.org/clapack/old/double/dgesv.c
!!


program image_Driver

  ! use LinAl, only: printMat2, readMat2, msize, nsize
  use LinAl

  implicit none
  character(len=100) :: myFileName
  ! .. Parameters ..
  INTEGER :: M, N
  INTEGER :: LDA, LDU, LDVT
  INTEGER :: LWMAX

  !.. Local Scalars ..
  INTEGER :: INFO, LWORK

  !.. Local Arrays ..
  ! DOUBLE PRECISION :: U( LDU, M ), VT( LDVT, N ), S( N ), WORK( LWMAX )
  double precision, dimension(:,:), allocatable :: A, U, VT, V, UT, U_copy
  double precision, dimension(:), allocatable :: S, WORK
  integer, dimension(8) :: ks
  integer :: k, i, j, m_size, n_size

  ! double precision, dimension(3) :: b


  ! .. Executable Statements ..


  ! Read data file 
  ! myFileName = 'dog_bw_data.dat'
  myFileName = 'dog_bw_data.dat'

  ! column count terminal command awk writes to file
  call execute_command_line("awk '{print NF}' dog_bw_data.dat | sort -nu | tail -n 1 > column_count.txt")
  open(10,file="column_count.txt")
  read(10,*) nsize
  close(10)

  ! row count terminal command wc writes to file
  call execute_command_line("wc -l < dog_bw_data.dat > row_count.txt")
  open(10,file="row_count.txt")
  read(10,*) msize
  close(10)


  print *, msize, nsize

  ! call readMatT(A, myFileName, m_size, n_size)
  call readMat2(myFileName)
  ! ! call printMat2(A)

  ! Define variables
  M = msize
  N = nsize ! we're storing the transpose
  LDA = M
  LDU = M
  LDVT = N
  LWMAX = max(3*min(M,N) + max(M,N), 5*min(M,N))
  ! LWMAX = 1000
  
!  ! Verify stuff
!  print *, shape(Data) 1270 (rows) x 1920 (columns)
!  print *, M ! 1920
!  print *, N ! 1279

  allocate(A(LDA, N))
  A = mat
  deallocate(mat)
  allocate(U(LDU, M))
  allocate(U_copy(LDU, M))
  allocate(VT(LDVT, N))
  U = 0.0
  VT = 0.0

  allocate(UT(M, M))
  allocate(V(N, N))

  allocate(S(N))
  S = 0.0
  allocate(WORK(LWMAX))


  ! Query the optimal workspace.
  LWORK = -1
  CALL DGESVD( 'A', 'A', M, N, A, LDA, S, U, LDU, VT, LDVT, WORK, LWORK, INFO )
  LWORK = MIN( LWMAX, INT( WORK( 1 ) ) )

  ! Compute SVD.
  CALL DGESVD( 'A', 'A', M, N, A, LDA, S, U, LDU, VT, LDVT, WORK, LWORK, INFO )

  ! Check for convergence.
  IF( INFO.GT.0 ) THEN
      WRITE(*,*)'The algorithm computing SVD failed to converge.'
      STOP
  END IF

! ! *     Print singular values.
  ks = (/20, 40, 80, 160, 320, 640, 1280, 2560/)

  ! First 10 singular values
  print *, "First 10 singular values"
  call print_vec(S(1:10))

  do i = 1,size(ks)
    k = ks(i)
    print *, "kth singular value: k = ", k
    print *, S(k)
    print *
  enddo

  ! Compute A_k using only the 
  ! ks = (/20, 40, 80, 160, 320, 640, 1280, 2560/)
  U_copy = U
  do j = 1,size(ks)
    ! print *, ks(j)
    k = ks(j)
    do i = 1,k
      U(:, i) = U(:, i) * S(i)
      ! V(:,i) = V(:,i) * S(i)
    enddo

    if ( k / 10 < 10) then
      write (myFileName, "(A3,I2,A4)") "Ak_", k, ".dat"
    else if (k/100 < 10) then
      write (myFileName, "(A3,I3,A4)") "Ak_", k, ".dat"
    else
      write (myFileName, "(A3,I4,A4)") "Ak_", k, ".dat"
    end if
    print *, trim(myFileName)

    print *, trim(myFileName)
    call printMatToFile(matmul(U(:,1:k), VT(1:k,:)), trim(myFileName))

    ! since U gets overwritten in loop multiplying by singular values
    U = U_copy
  enddo

  !!!!!!!!!!!!! This worked !!!!!!!!!!!!!!!!!
  ! k = 3355
  ! do i = 1,k
  !   U(:, i) = U(:, i) * S(i)
  ! enddo
  ! write (myFileName, "(A3,I4,A4)") "Ak_", k, ".dat"
  ! call printMatToFile(matmul(U(:,1:k), VT(1:k,:)), trim(myFileName))
  !!!!!!!!!!!!! This worked !!!!!!!!!!!!!!!!!

end program image_Driver
