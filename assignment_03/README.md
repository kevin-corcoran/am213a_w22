## Code for part 1

The driver code for both the Cholesky solution to least squares, and the
Householder QR decomposition of the Vandermonde matrix is located in the
*./part1_numeric/* directory in the *Driver_LinAl.f90* file

To run the code
```bash
cd part1_numeric/
```

compile with 
```bash
make
```

then the output is located in the text file

```bash
part1_numeric/output.txt
```

The plotting routine is located in

```bash
part1_numeric/plotter.jl
```

This file reads from *interp3.dat* and *interp5.dat* for Cholesky decomposition
and *interp3_house.dat* and *interp5_house.dat* for the Householder QR
solution to the least squares problem.

I manually changed line 58 in *Driver_LinAl.f90* to change the degree of the
polynomial interpolant
```bash
  d = 5 ! degree of polynomial
```

The filenames were also manually changed on line 124 for the Cholesky solution
```bash
  open(10,file="interp5.dat")
```

and line 227 for the Householder QR solution
```bash
  open(10,file="interp5_house.dat")
```

The subroutine householderQR(A, d, dimsA) replaces the matrix input matrix A with the
upper triangular matrix R and the normalized householder vectors are stored in
the lower half of A. The diagonal elements of R are stored in the vector d.

The subroutine householderQy(A,y,dimsA) and subroutine householder Qty(A_qr,
y), calculates Qy and Q^Ty respectively replacing y in the process.

The subroutine expliciteQR(A_qr, dR, Q, R) takes the matrix replaced by
householderQR, and the diagonal elements of R in dR, and explicitly calculates
Q and R. The matrix Q is calculated by calling householderQy on the columns of the
identity matrix, and R us unpacked from the upper triangular part of A and the
diagonal elements stored in dR.

