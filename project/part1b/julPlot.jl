using Plots

jac = "jac_error_d"
gseid = "gseid_error_d"
conjgr = "conjgr_error_d"

J = [] # hold Jacobi iteration error
GS = [] # hold Gauss-Seidel iteration error
CG = [] # hold Conjugate Gradient iteration error


ds = [2 5 10 100 1000]
for d in ds
    J = [] # hold Jacobi iteration error
    GS = [] # hold Gauss-Seidel iteration error
    CG = [] # hold Conjugate Gradient iteration error

    # Read data and store in arrays J, GS, CG
    filename1 = string(jac, d, ".dat")
    data1 = readlines(filename1)

    filename2 = string(gseid, d, ".dat")
    data2 = readlines(filename2)

    # filename3 = string(conjgr, d, ".dat")
    # data3 = readlines(filename3)

    for i = 1:length(data1)
        append!(J, parse(Float64, data1[i]))
    end
    for i = 1:length(data2)
        append!(GS, parse(Float64, data2[i]))
    end
    # for i = 1:length(data3)
    #     append!(CG, parse(Float64, data3[i]))
    # end

    # Plot data on a log log plot
    # l = length(j) # number of iterations in alg.
    # p1 = plot(1:l, j, xaxis=:log, yaxis=:log, xlabel = "iterations", ylabel = "error") 
    # l = length(gs)
    # p2 = plot(1:l, gs, xaxis=:log, yaxis=:log, xlabel = "iterations", ylabel = "error")
    # l = length(cg)
    # p3 = plot(1:l, cg, xaxis=:log, yaxis=:log, xlabel = "iterations", ylabel = "error")

    l = length(J) # number of iterations in alg.
    plot(1:l, J, xaxis=:log, xlabel = "iterations", ylabel = "error", label = "Jacobi") 
    title!(string("D = ", d))
    l = length(GS)
    display(plot!(1:l, GS, xaxis=:log, xlabel = "iterations", ylabel = "error", label = "Gauss-Seidel"))
    savefig(string("error_", d))

    # display(plot(p1,p2,p3, layout = 3, title = ["jacobi" "gauss-seidel" "conjugate gradient"], label = [string("d = ",d) string("d = ",d) string("d = ",d)]))
end

# Plot error when a_ii = i
filename = "jac_error.dat"
data = readlines(filename)
A = []
for i = 1:length(data)
    append!(A, parse(Float64, data[i]))
end
l = length(A) # number of iterations in alg.
plot(1:l, A, xaxis=:log, xlabel = "iterations", ylabel = "error", label = "Jacobi") 

filename = "gseid_error.dat"
data = readlines(filename)
A = []
for i = 1:length(data)
    append!(A, parse(Float64, data[i]))
end
l = length(A) # number of iterations in alg.
scatter!(1:l, A, xaxis=:log, xlabel = "iterations", ylabel = "error", label = "Gauss-Seidel") 
title!("a_ii = i")

savefig("a_ii")