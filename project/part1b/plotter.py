import numpy as np
import matplotlib.pyplot as plt

Ah = np.loadtxt("dog_bw_data.dat")
fig0 = plt.figure(figsize=(25,10))
plt.imshow(Ah, cmap="gray")
plt.title("Original")

A0 = np.loadtxt("dog_bw_data_LR.dat")
fig1 = plt.figure(figsize=(25,10))
plt.imshow(A0, cmap="gray")
plt.title("Low resolution")

m, n = np.shape(A0)

ks = [20, 40, 80, 160, 320, 640]
fig = plt.figure(figsize=(25,10))

i = 0
E = [] # norms
for k in ks:
    i = i + 1

    filename = f'Ak_{k}.dat'
    A = np.loadtxt(filename)

    l = len(A)
    A = A.reshape(m, n) #, order='C') # 'C', 'F', 'A'

    E.append(np.linalg.norm(A-A0)/k)

    plt.subplot(2,3, i)
    plt.imshow(A, cmap="gray")
    plt.title(f"Rank {k} Approximation")

plt.show()
