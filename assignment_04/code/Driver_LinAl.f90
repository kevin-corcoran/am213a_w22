! TODO: make separate drivers for part 1 (problem 1 and 2)
Program Driver_LinAl

  use LinAl

  implicit none
  
  character(len=100) :: myFileName
  integer :: i, j

  real, dimension(:,:), allocatable :: A, B1, B2, C, v

  ! Variables for coding problem 1 (cholesky solution of least squares)
  ! TODO: change n -> m, add: n = d+1
  integer :: m, n ! mxn size
  real :: norm

  ! real :: err, a, b
  integer :: N_p
  logical :: SINGLR
  logical :: flag
  SINGLR = .FALSE.
  flag = .FALSE.

  print *, "1. Tridiagonal"
  myFileName = 'Amat.dat'
  call read_mat(A, myFileName)
  call print_mat(A)
  call hessenberg(A)
  call print_mat(A)

  print *, "2. Eigenvalues"
  print*, "i Without shift"
  myFileName = 'Bmat.dat'
  call read_mat(B1, myFileName)
  call print_mat(B1)
  ! print *, "Hessenberg"
  ! call hessenberg(B1)
  ! call print_mat(B1)
  call eigQR(B1)
  call print_mat(B1)
  
  print *, "ii With shift"
  call read_mat(B2, myFileName)
  call print_mat(B2)
  ! print *, "Hessenberg"
  ! call hessenberg(B2)
  ! call print_mat(B2)
  call eigQRshift(B2)
  print *, "After QR shift"
  call print_mat(B2)

  print *, "3. Eigenvectors"
  myFileName = 'Cmat.dat'
  call read_mat(C, myFileName)
  call print_mat(C)

  ! "initial" eigenvector 
  m = size(C,dim=1)
  allocate(v(m,1))

  v = 1.0/sqrt(m+0.0)


  print *, "est: -8"
  call inverseIter(C, -8.0, v)
  call print_mat(v)

  ! change "initial" eigenvector back
  v = 1.0/sqrt(m+0.0)
  print *, "est: 7"
  call inverseIter(C, 7.5, v)
  call print_mat(v)

  v = 1.0/sqrt(m+0.0)
  print *, "est: 5"
  call inverseIter(C, 5.5, v)
  call print_mat(v)

  v = 1.0/sqrt(m+0.0)
  print *, "est: -1.5"
  call inverseIter(C, -1.5, v)
  call print_mat(v)

end Program Driver_LinAl
